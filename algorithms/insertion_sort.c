#include <stdio.h>

int main()
{
  int prices[] = {
    60000,
    16000,
    46000,
    1000000000,
    17000,
    123092
  };

  int INT_SIZE = 4;
  int n = sizeof(prices) / INT_SIZE;

  for (int i = 1; i < n; i++)
  {
    for (int j = i; j > 0; j--)
    {
      int actual_price = prices[j];
      int previous_price = prices[j -1];

      if (actual_price < previous_price) {
        prices[j] = previous_price;
        prices[j -1] = actual_price;

        continue;
      }

      break;
    }
  }

  for (int i = 0; i < n; i++)
  {
    printf("[%d]: %d \n", i, prices[i]);
  }

}
