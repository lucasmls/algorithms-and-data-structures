#include <stdio.h>

int main()
{
  int prices[] = {
    60000,
    16000,
    46000,
    1000000000,
    17000,
    123092
  };

  int INT_SIZE = 4;
  int n = sizeof(prices) / INT_SIZE;

  for (int i = 0; i < n; i++)
  {
    for (int j = i + 1; j < n; j++)
    {
      int actual_value = prices[i];
      int next_value = prices[j];

      if (actual_value > next_value)
      {
        prices[i] = next_value;
        prices[j] = actual_value;
      }
      
    }
  }

  for (int i = 0; i < n; i++)
  {
    printf("[%d]: %d \n", i, prices[i]);
  };
}