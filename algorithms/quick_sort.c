#include <stdio.h>

void swap(int items[], int from, int to)
{
  int first_item = items[from];
  int second_item = items[to];

  items[from] = second_item;
  items[to] = first_item;
}

int break_on_pivot(int items[], int start, int end)
{
  int smallers_found = 0;
  int pivot = items[end -1];

  for (int i = 0; i < end -1; i++)
  {
    int actual = items[i];
    if (actual <= pivot)
    {
      swap(items, i, smallers_found);
      smallers_found++;
    }
  }
  
  swap(items, end -1, smallers_found);
  return smallers_found;
}

void quick_sort(int items[], int from, int to)
{
  int elements = to - from;
  if (elements > 1)
  {
    int pivot_position = break_on_pivot(items, from, to);
    quick_sort(items, from, pivot_position);
    quick_sort(items, pivot_position + 1, to);
  }
}

int main()
{
  int items[] = { 9, 8, 4, 10, 3, 6, 9, 5, 7 };
  int items_size = sizeof(items) / sizeof(items[0]);

  quick_sort(items, 0, items_size);

  for (int i = 0; i < items_size; i++)
  {
    printf("[%d]: %d \n", i, items[i]);
  }
  
}
