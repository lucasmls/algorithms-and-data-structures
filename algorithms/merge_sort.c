#include <stdio.h>

void merge(float items[], int start, int mid, int end)
{
  float sorted_slice[end - start];

  int i = 0;
  int a_i = start;
  int b_i = mid;

  while (a_i < mid && b_i < end)
  {
    float a = items[a_i];
    float b = items[b_i];

    if (a < b)
    {
      sorted_slice[i] = a;
      a_i++;
    }
    else
    {
      sorted_slice[i] = b;
      b_i++;
    }

    i++;
  }

  while (a_i < mid)
  {
    sorted_slice[i] = items[a_i];
    a_i++;
    i++;
  }

  while (b_i < end)
  {
    sorted_slice[i] = items[b_i];
    b_i++;
    i++;
  }

  for (int j = 0; j < i; j++)
  {
    items[start + j] = sorted_slice[j];
  }
}

void merge_sort(float items[], int start, int end)
{
  int qtd = end - start;

  if (qtd > 1)
  {
    int mid = (start + end) / 2;
    merge_sort(items, start, mid);
    merge_sort(items, mid, end);
    merge(items, start, mid, end);
  }
}

int main()
{
  float items[] = {
    9.3,
    8.5,
    4.0,
    10.0,
    3.0,
    6.7,
    7.0,
    9.0,
    5.0,
  };

  int items_size = sizeof(items) / sizeof(items[0]);

  merge_sort(items, 0, items_size);

  for (int it = 0; it < items_size; it++)
  {
    printf("[%d]: %f \n", it, items[it]);
  }

}