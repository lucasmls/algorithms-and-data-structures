#include <stdio.h>

/**
 * Search a sorted array by repeatedly dividing the search interval in half.
 * Begin with an interval covering the whole array. If the value of the search key is less
 * than the item in the middle of the interval,  narrow the interval to the lower half.
 * Otherwise narrow it to the upper half. Repeatedly check until the value is found or the interval is empty.
 *
 * Time complexity: O(Log n)
 */

int binary_search(int items[], int start, int end, int number_to_find)
{
  int middle = (start + end) / 2;
  int middle_item = items[middle];

  if (start > end)
  {
    return -1;
  }

  if (number_to_find == middle_item)
  {
    return middle;
  }

  if (number_to_find < middle_item)
  {
    return binary_search(items, start, middle - 1, number_to_find);
  }

  return binary_search(items, middle + 1, end, number_to_find);
}

int main()
{
  int items[] = {3, 3, 4, 5, 6, 7, 8, 9, 9, 10};

  int found_index = binary_search(items, 0, 10, 8);
  printf("Found: %d", found_index);
}