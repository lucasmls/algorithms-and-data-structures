#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "linked-list.h"

/**
 * @brief Creates a linked list.
 */
linked_list create_linked_list()
{
  linked_list list;
  list.total = 0;
  list.head = NULL;
  list.tail = NULL;

  return list;
}

/**
 * @brief Inserts a node into the first position of the linked list.
 */
void list_insert_first(linked_list* list, int value)
{
  if (list->total == 0)
  {
    struct node *new_node = (struct node*) malloc(sizeof(struct node));
    new_node->value = value;
    new_node->next = NULL;
    new_node->previous = NULL;

    list->head = new_node;
    list->tail = new_node;

    list->total++;

    return;
  }

  struct node *new_node = (struct node*) malloc(sizeof(struct node));
  new_node->value = value;
  new_node->next = list->head;

  list->head->previous = new_node;
  list->head = new_node;

  list->total++;
}

/**
 * @brief Inserts a node into the last position of the linked list.
 */
void list_insert_last(linked_list* list, int value)
{
  if (list->total == 0)
  {
    insert_first(list, value);
    return;
  }

  struct node *new_node = (struct node*) malloc(sizeof(struct node));

  new_node->value = value;
  new_node->next = NULL;
  new_node->previous = list->tail;

  list->tail->next = new_node;
  list->tail = new_node;

  list->total++;
}

/**
 * @brief Inserts a node into the specified position of the linked list.
 */
void list_insert_in(linked_list* list, int value, int position)
{
  if (position == 0)
  {
    insert_first(list, value);
    return;
  }

  if (position == list->total -1)
  {
    insert_last(list, value);
    return;
  }

  struct node *previous_node = find(list, position -1);
  struct node *next_node = previous_node->next;

  struct node *new_node = (struct node*) malloc(sizeof(struct node));
  new_node->value = value;

  new_node->next = next_node; 
  new_node->previous = previous_node; 

  previous_node->next = new_node;
  next_node->previous = new_node;

  list->total++;
}

/**
 * @brief Returns if the specified position exists or not.
 */
bool list_is_valid_position(linked_list* list, int position)
{
  return position >= 0 && position < list->total;
}

/**
 * @brief Find the specified node.
 */
node* list_find(linked_list* list, int position)
{
  if (!is_valid_position(list, position))
  {
    exit(0);
  }

  struct node *actual_node = list->head;
  for (int i = 0; i < position; i++)
  {
    actual_node = actual_node->next;
  }
  
  return actual_node;
}

/**
 * @brief Removes the first node of the linked list.
 */
void list_remove_first(linked_list* list)
{
  if (list->total == 0)
  {
    exit(0);
  }
  
  list->head = list->head->next;
  list->total--;

  if (list->total == 0)
  {
    list->tail = NULL;
  }
}

/**
 * @brief Removes the last node of the linked list.
 */
void list_remove_last(linked_list* list)
{
  if (list->total == 0)
  {
    remove_first(list);
    return;
  }

  struct node *penultimate_node = list->tail->previous;
  penultimate_node->next = NULL;
  list->tail = penultimate_node;
  list->total--;
}

/**
 * @brief Removes the specified node of the linked list.
 */
void list_remove_from(linked_list* list, int position)
{
  if (position == 0)
  {
    remove_first(list);
    return;
  }

  if (position == list->total -1)
  {
    remove_last(list);
    return;
  }

  struct node *node_to_remove = find(list, position);

  struct node *previous_node = node_to_remove->previous;
  struct node *next_node = node_to_remove->next;

  previous_node->next = node_to_remove->next;
  next_node->previous = node_to_remove->previous;

  list->total--;
}

/**
 * @brief Returns true or false if the value are included into the list.
 */
bool list_includes(linked_list* list, int value)
{
  struct node *actual = list->head;

  while (actual->next != NULL)
  {
    if (actual->value == value)
    {
      return true;
    }

    actual = actual->next;
  }
  
  return false;
}

/**
 * @brief Returns the linked list size.
 */
int list_size(linked_list* list)
{
  return list->total;
}

/**
 * @brief Prints the linked list.
 */
void list_print(linked_list* list)
{
  struct node *ptr = list->head;

  while(ptr != NULL) {
    printf("%d ", ptr->value);
    ptr = ptr->next;
  }

  printf("\n");
}