#ifndef __LINKED_LIST__
  #define __LINKED_LIST__

  /**
   * @brief Node is the item itself of the linked list.
   */
  typedef struct node
  {
    int value;
    struct node *next;
    struct node *previous;
  } node;

  /**
   * @brief Linked list is the struct that holds the list data.
   * @param total is the total of elements that are included in the linked list.
   * @param head is the first item of the linked list.
   * @param tail is the last item of the linked list.
   */
  typedef struct linked_list
  {
    int total;
    node *head;
    node *tail;
  } linked_list;

  /**
   * @brief Creates a linked list.
   */
  linked_list
  create_linked_list();

  /**
   * @brief Inserts a node into the first position of the linked list.
   */
  void
  list_insert_first(linked_list* list, int value);

  /**
   * @brief Inserts a node into the last position of the linked list.
   */
  void
  list_insert_last(linked_list* list, int value);

  /**
   * @brief Inserts a node into the specified position of the linked list.
   */
  void
  list_insert_in(linked_list* list, int value, int position);

  /**
   * @brief Returns if the specified position exists or not.
   */
  bool
  list_is_valid_position(linked_list* list, int position);

  /**
   * @brief Find the specified node.
   */
  struct node*
  list_find(linked_list* list, int position);

  /**
   * @brief Removes the first node of the linked list.
   */
  void
  list_remove_first(linked_list* list);

  /**
   * @brief Removes the last node of the linked list.
   */
  void
  list_remove_last(linked_list* list);

  /**
   * @brief Removes the specified node of the linked list.
   */
  void
  list_remove_from(linked_list* list, int position);

  /**
   * @brief Returns true or false if the value are included into the list.
   */
  bool
  list_includes(linked_list* list, int value);

  /**
   * @brief Returns the linked list size.
   */
  int
  list_size(linked_list* list);

  /**
   * @brief Prints the linked list.
   */
  void
  list_print(linked_list* list);

#endif