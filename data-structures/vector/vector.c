#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "vector.h"

/**
 * @brief Creates a vector.
 */
vector_t vector_create(int size)
{
  vector_t v;

  v.capacity = size;
  v.total = 0;
  v.items = malloc(sizeof(int) * size);

  return v;
}

/**
 * @brief Adds a new item into the vector.
 */
void vector_add(vector_t* v, int item)
{
  vector_resize(v);
  v->items[v->total++] = item;
}

/**
 * @brief Adds a new item into a specific vector position.
 */
void vector_add_in(vector_t* v, int item, int position)
{
  vector_resize(v);

  for (int i = v->total -1; i >= position; i--)
  {
    v->items[i + 1] = v->items[i];
  }

  v->items[position] = item;
  v->total++;
}

/**
 * @brief Removes a item into from a specific vector position.
 */
void vector_remove(vector_t* v, int position)
{
  for (int i = position; i < v->total; i++)
  {
    v->items[i] = v->items[i + 1];
  }

  v->total--;
}

/**
 * @brief Returns the specified item.
 */
int vector_find(vector_t* v, int index)
{
  return v->items[index];
}

/**
 * @brief Doubles the vector allocated memory.
 */
void vector_resize(vector_t* v)
{
  if (v->capacity == v->total)
  {
    v->items = realloc(v->items, sizeof(int) * (v->capacity * 2));
    v->capacity = v->capacity * 2;
  }
}

/**
 * @brief Returns the items quantity in the vector.
 */
int vector_size(vector_t* v)
{
  return v->total;
}

/**
 * @brief Returns if the specified item are included in the vector.
 */
bool vector_includes(vector_t* v, int item)
{
  for (int i = 0; i < v->total; i++)
  {
    if (v->items[i] == item)
    {
      return true;
    }
  }
  
  return false;
}
