#include <stdbool.h>

#ifndef __VECTOR_H__

  #define __VECTOR_H__

  /**
   * @brief Vector's structure.
   */
  typedef struct vector_t {
    int capacity;
    int total;
    int *items;
  } vector_t;

  /**
   * @brief Creates a vector.
   */
  vector_t
  vector_create(int size);

  /**
   * @brief Adds a new item into the vector.
   */
  void
  vector_add(vector_t* v, int item);

  /**
   * @brief Adds a new item into a specific vector position.
   */
  void
  vector_add_in(vector_t* v, int item, int position);

  /**
   * @brief Removes a item into from a specific vector position.
   */
  void
  vector_remove(vector_t* v, int position);

  /**
   * @brief Returns the specified item.
   */
  int
  vector_find(vector_t* v, int index);

  /**
   * @brief Doubles the vector allocated memory.
   */
  void
  vector_resize(vector_t* v);

  /**
   * @brief Returns the items quantity in the vector.
   */
  int
  vector_size(vector_t* v);

  /**
   * @brief Returns if the specified item are included in the vector.
   */
  bool
  vector_includes(vector_t* v, int item);

#endif