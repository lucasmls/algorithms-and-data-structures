#ifndef __STACK_H__
#define __STACK_H__

  typedef int stack_item;

  /**
   * @brief t_stack is the struct that holds the stack data.
   * @param max is the maximum amount of elements that can be included in the stack.
   * @param top is the reference of the last included item in the stack.
   * @param items is the items that are included in the stack.
   */
  typedef struct t_stack {
    int max;
    int top;
    stack_item *items;
  } t_stack;

  /**
   * @brief Creates a stack.
   */
  t_stack
  create_stack(int max);

  /**
   * @brief Push a new item into the stack.
   */
  void
  stack_push(t_stack* stack, int item);

  /**
   * @brief Pops the latest included item of the stack.
   */
  int
  stack_pop(t_stack* stack);

  /**
   * @brief Returns the top item of the stack.
   */
  int
  stack_peek(t_stack* stack);

  /**
   * @brief Returns true or false if the stack is full.
   */
  bool
  stack_is_full(t_stack* stack);

  /**
   * @brief Returns true or false if the stack is empty.
   */
  bool
  stack_is_empty(t_stack* stack);

  /**
   * @brief Clears out the whole stack.
   */
  void
  stack_clear(t_stack* stack);
  
#endif