#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "stack.h"

/**
 * @brief Creates a stack.
 */
t_stack create_stack(int max)
{
  t_stack stack;

  stack.max = max;
  stack.top = -1;
  stack.items = malloc(sizeof(char) * max);

  return stack;
}

/**
 * @brief Returns the top item of the stack.
 */
int stack_peek(t_stack* stack)
{
  if (is_empty(stack))
  {
    fprintf(stderr, "Can't peek a item from an stack that are empty. \n");
    exit(1);
  }

  return stack->items[stack->top];
}

/**
 * @brief Push a new item into the stack.
 */
void stack_push(t_stack* stack, int item)
{
  if (is_full(stack))
  {
    fprintf(stderr, "Can't push a item into a stack that are full. \n");
    exit(1);
  }

  stack->items[++stack->top] = item;
}

/**
 * @brief Pops the latest included item of the stack.
 */
int stack_pop(t_stack* stack)
{
  if (is_empty(stack))
  {
    fprintf(stderr, "Can't pop a item from an stack that are empty. \n");
    exit(1);
  }

  int peeked = peek(stack);
  stack->top--;
  return peeked;
}

/**
 * @brief Returns true or false if the stack is full.
 */
bool stack_is_full(t_stack* stack)
{
  return (stack->top == (stack->max - 1));
}

/**
 * @brief Returns true or false if the stack is empty.
 */
bool stack_is_empty(t_stack* stack)
{
  return stack->top == -1;
}

/**
 * @brief Clears out the whole stack.
 */
void stack_clear(t_stack* stack)
{
  free(stack->items);
  stack->top = -1;
}